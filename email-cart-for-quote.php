<?php
/**
 * Plugin Name: Email Cart
 * Description: Plugin to email the cart to the specified email address
 * Version: 1.0
 * Author: David Gardner
 */
// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

add_action('init','script_cart_email_setup');
add_action('wp_enqueue_scripts', 'script_cart_email_setup');

function script_cart_email_setup() {
    wp_localize_script( 'email_cart_jquery', 'obj',
            array( 'url' => admin_url( 'admin-ajax.php' ) ) );
    wp_enqueue_script( 'email_cart_jquery', plugins_url('/assets/js/scripts.js', __FILE__), array('jquery') ,'1.12.4' );
}

function get_cart_html(){
    do_action( 'woocommerce_before_cart' ); 
    $cart = WC()->cart->get_cart();
    $count = 0;
    $th_style=' style="text-align:left;padding: 12px;background:rgb(33, 82, 158); color: #fff;"';
    $product_table_html = 
    '<table class="shop_table" cellspacing="0">
       <thead>
          <tr>
             <th class="product-name"'.$th_style.'>Product</th>
             <th class="product-quantity"'.$th_style.'>Quantity</th>
             <th class="product-desc"'.$th_style.'>Product Description</th>
          </tr>
       </thead>
       <tbody>';
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
       $count+=1;
       $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
         $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
       if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
          $alt = $count % 2 ? ' style="background-color: #f2f2f2; border-bottom: 1px solid #ddd;"':' style="border-bottom: 1px solid #ddd;"';
          $cell_style=' style="padding: 12px;"';
          $item_name = $_product->get_name();
          do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
          $addition_details = '<br/><div style="margin-left:20px">'.wc_get_formatted_cart_item_data( $cart_item ).'</div>';
          
          $product_table_html.= 
             '<tr class="product-row"'.$alt.'>
                <td valign="top"'.$cell_style.'>'
                  .$item_name.$addition_details.
                '</td>
                <td valign="top"'.$cell_style.'>'.$cart_item['quantity'].'</td>
                <td valign="top"'.$cell_style.'>'.$_product->get_short_description().'</td>
             </tr>';
       }
    }
    $html = '<div class="cart-email-wrapper">';
    $html.= '<h1 class="cart-title" style="text-align:center">Total Items for Quote: '.$count.'</h1>';
    $html.= $product_table_html . '</tbody></table>';
    $html.='</div>';
    return $html;
 }
 
 add_action( 'wp_ajax_email_quote_action', 'email_quote_action' );
 function email_quote_action() {
    $current_user = wp_get_current_user();
    if(! $current_user->exists()){
       echo 'There was an error fetching your email address. Please sign up or login and try again.';
    }
    else{
        $recipient = '';
        $subject = 'Request for Quotation';
        $headers = array('From: '.$current_user->user_firstname.' '.$current_user->user_lastname.' <'.$current_user->user_email.'>',
        'Content-Type: text/html; charset=UTF-8');
        $message = get_cart_html() ;
        $sent = wp_mail( $recipient, $subject, $message, $headers);
        if($sent){
            echo 'sent';
         }
         else{
            echo 'error';
         }
    }
    wp_die(); // this is required to terminate immediately and return a proper response
 }
