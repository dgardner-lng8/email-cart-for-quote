# Email Cart for Quote

Custom WordPress plugin to allow user's cart to be emailed instead of submitted for purchase. The Cart is then saved on the user's end until otherwise updated.

V.1: Compatible with WordPress versions: 5.0 and up. Not yet tested with version 5.3

## TODO: Update script to automatically inject email cart button. Currently WooCommerce cart.php template needs to be overridden to remove the checkout buttons and add the email cart button

## TODO: create admin interface to set user preferences for the email cart recipient, cc and bcc fields.