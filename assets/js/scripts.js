jQuery(document).ready(
    function( $ ){
       var is_blocked = function( $node ) {
           return $node.is( '.processing' ) || $node.parents( '.processing' ).length;
       };

       var block = function( $node ) {
           if ( ! is_blocked( $node ) ) {
               $node.addClass( 'processing' ).block( {
                   message: null,
                   overlayCSS: {
                       background: '#fff',
                       opacity: 0.6
                   }
               } );
           }
       };

       var unblock = function( $node ) {
           $node.removeClass( 'processing' ).unblock();
       };

       var show_notice = function( html_element, $target ) {
           if ( ! $target ) {
               $target = jQuery( '.woocommerce-notices-wrapper:first' ) || jQuery( '.cart-empty' ).closest( '.woocommerce' ) || jQuery( '.woocommerce-cart-form' );
           }
           $target.prepend( html_element );
       };

       jQuery('.woocommerce-cart-form').submit(
           function(evt){
               var $submit  = jQuery( document.activeElement ),
               $clicked = jQuery( ':input[type=submit][clicked=true]' ),
               $form    = jQuery( evt.currentTarget);

               //cart to get functions

               if ( is_blocked( $form ) ) {
                   return false;
               }

               if ( $clicked.is( ':input[name="email_list"]' ) ) {
                   evt.preventDefault();
                   block( $form );
                   // Make call to actual form post URL.
                   var data ={
                       'action': 'email_quote_action',
                   };
                   $.ajax( {
                       type:     $form.attr( 'method' ),
                       url:      obj.url,
                       data:     data,
                       success:  function( response ) {
                           if(response.includes('sent')){
                               $( '.woocommerce-error, .woocommerce-message, .woocommerce-info' ).remove();
                               show_notice('<div class="woocommerce-message" role="alert">Quotation sent. Please look out for a response email in your inbox.</div>')
                           }
                           else{
                               $( '.woocommerce-error, .woocommerce-message, .woocommerce-info' ).remove();
                               show_notice('<div class="woocommerce-error" role="alert">Error sending qutation. Please try again later.</div>')
                           }
                       },
                       error: function(response){
                           $( '.woocommerce-error, .woocommerce-message, .woocommerce-info' ).remove();
                           show_notice('<div class="woocommerce-error" role="alert">Error sending qutation. Please try again later. If you are not logged in: please log in and try again.</div>')
                           
                       },
                       complete: function(response) {
                           $( '.woocommerce-cart-form' ).find( ':input[name="update_cart"]' ).prop( 'disabled', true );
                           unblock( $form );
                           //disable submit for quote
                           $.scroll_to_notices( $( '[role="alert"]' ) );
                       }
                   } );    
               }
           }      
       );  
    }
);